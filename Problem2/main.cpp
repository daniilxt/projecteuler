#include <iostream>
using namespace std;

void Fibonacci (int& x1, int& x2)
{
  int tmp = x2;
  x2 = x1 + x2;
  x1 = tmp;
}

int main() {
  int x1 = 0;
  int x2 = 1;
  int result = 0;
  while (x2 < 4000000)
  {
    if(x2 % 2 == 0)
    {
      result += x2;
    }
    Fibonacci(x1, x2);
  }
  cout << result;
  return 0;
}